import { Frames } from "../frames";
import { FrameCoordinate } from "../frame-coordinate";
import { StaticAnimation } from "../animations/static-animation-";

export class Background extends StaticAnimation{

    constructor(context: CanvasRenderingContext2D){
        super(context); 
        this.createSprite();   
    }

    private createSprite(){
        this.frames.texture.src = '../assets/images/background/background1.jpg';
        this.positionX.push(0); this.positionY.push(0);
        this.frames.coordinate.push(new FrameCoordinate(0, 0, window.innerWidth, window.innerHeight));
    }
}