import { Frames } from "../frames";
import { FrameCoordinate } from "../frame-coordinate";
import { StaticAnimation } from "../animations/static-animation-";

export class Ground extends StaticAnimation {
    private tileWidth: number = 112;
    private tileHeight: number = 112;
    
    constructor(context: CanvasRenderingContext2D){
        super(context);
        this.createSprite();
    }

    private createSprite(){
        this.frames.texture.src = '../assets/images/ground/ground.png';
        this.generateGround();
        this.generateHigherGround();
    }

    private generateGround(){
        this.frames.coordinate.push(new FrameCoordinate(40, 396, this.tileWidth, this.tileHeight));
        this.positionX.push(0); this.positionY.push(657);

        for(let i = 0; i < 12; i++){
            this.frames.coordinate.push(new FrameCoordinate(40 + this.tileWidth, 396, this.tileWidth, this.tileHeight));
            this.positionX.push((i + 1) * this.tileWidth); this.positionY.push(656);
        }

        this.frames.coordinate.push(new FrameCoordinate(40 + 2*this.tileWidth, 396, this.tileWidth, this.tileHeight));
        this.positionX.push(0 + 13*this.tileWidth); this.positionY.push(656);
    }

    private generateHigherGround(){
        this.frames.coordinate.push(new FrameCoordinate(637, 424, this.tileWidth, this.tileHeight));
        this.positionX.push(30); this.positionY.push(450);
        this.frames.coordinate.push(new FrameCoordinate(689, 625, this.tileWidth, this.tileHeight));
        this.positionX.push(30 + this.tileWidth); this.positionY.push(450);
        this.frames.coordinate.push(new FrameCoordinate(689, 625, this.tileWidth, this.tileHeight));
        this.positionX.push(30 + 2*this.tileWidth); this.positionY.push(450);
        this.frames.coordinate.push(new FrameCoordinate(800, 425, this.tileWidth, this.tileHeight));
        this.positionX.push(30 + 3*this.tileWidth); this.positionY.push(450);

        this.frames.coordinate.push(new FrameCoordinate(637, 424, this.tileWidth, this.tileHeight));
        this.positionX.push(600); this.positionY.push(250);
        this.frames.coordinate.push(new FrameCoordinate(800, 425, this.tileWidth, this.tileHeight));
        this.positionX.push(600 + this.tileWidth); this.positionY.push(250);

        this.frames.coordinate.push(new FrameCoordinate(637, 424, this.tileWidth, this.tileHeight));
        this.positionX.push(1050); this.positionY.push(200);
        this.frames.coordinate.push(new FrameCoordinate(689, 625, this.tileWidth, this.tileHeight));
        this.positionX.push(1050 + this.tileWidth); this.positionY.push(200);
        this.frames.coordinate.push(new FrameCoordinate(689, 625, this.tileWidth, this.tileHeight));
        this.positionX.push(1050 + 2*this.tileWidth); this.positionY.push(200);
        this.frames.coordinate.push(new FrameCoordinate(800, 425, this.tileWidth, this.tileHeight));
        this.positionX.push(1050 + 3*this.tileWidth); this.positionY.push(200);     
    }
}