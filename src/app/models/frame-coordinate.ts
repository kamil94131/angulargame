export class FrameCoordinate {
    private _x: number;
    private _y: number;
    private _width: number;
    private _height: number;
	
	constructor(x: number, y: number, width: number, height: number) {
		this._x = x;
		this._y = y;
		this._width = width;
		this._height = height;
	}

	public get x(): number {
		return this._x;
	}

	public set x(x: number) {
		this._x = x;
	}
    
	public get y(): number {
		return this._y;
	}

	public set y(y: number) {
		this._y = y;
	}

	public get width(): number {
		return this._width;
	}

	public set width(width: number) {
		this._width = width;
	}

	public get height(): number {
		return this._height;
	}

	public set height(height: number) {
		this._height = height;
    }  
}
