import { FrameCoordinate } from '../frame-coordinate';
import { DynamicAnimationContinuous } from "../animations/dynamic-animation-continuous";
import { MoveVector } from '../move-vector';

export class Coin extends DynamicAnimationContinuous {
    private moveVector: MoveVector = new MoveVector();
    private endTime: number = 0;
    private speed: number = 3;
    public spriteWidth: number = 64;
    public spriteHeight: number = 64;
    private stopY: number = 0;

    constructor(context: CanvasRenderingContext2D){
        super(context);
        this.configureSprite();
        this.loadFrames();
    }

    private configureSprite(): void{
        this.positionX = Math.floor(Math.random() * 1400) + 10;
        this.positionY = 0;
        this.c();
        this.frameTime = 1;
    }

    private calculateStopY(x: number): number{
        if(x>= 30 && x <=464)
            return 450;
        else if(x>= 628 && x <=800)
            return 250
        else if(x>= 1070 && x <=1478)
            return 200;
        else
            return 657;
    }

    public c(){
        this.stopY = this.calculateStopY(this.positionX) - this.spriteHeight;
    }

    public loadFrames(): void{
        this.frames.texture.src = '../assets/images/coins/coin.png';
        this.frames.coordinate.push(new FrameCoordinate(0 , 0, this.spriteWidth, this.spriteWidth));
        this.frames.coordinate.push(new FrameCoordinate(this.spriteWidth , 0, this.spriteWidth, this.spriteWidth));
        this.frames.coordinate.push(new FrameCoordinate(2*this.spriteWidth , 0, this.spriteWidth, this.spriteWidth));
        this.frames.coordinate.push(new FrameCoordinate(3*this.spriteWidth , 0, this.spriteWidth, this.spriteWidth));
        this.frames.coordinate.push(new FrameCoordinate(4*this.spriteWidth , 0, this.spriteWidth, this.spriteWidth));
        this.frames.coordinate.push(new FrameCoordinate(5*this.spriteWidth , 0, this.spriteWidth, this.spriteWidth));
        this.frames.coordinate.push(new FrameCoordinate(6*this.spriteWidth , 0, this.spriteWidth, this.spriteWidth));
        this.frames.coordinate.push(new FrameCoordinate(7*this.spriteWidth , 0, this.spriteWidth, this.spriteWidth));

        this.frames.coordinate.push(new FrameCoordinate(0 , this.spriteWidth, this.spriteWidth, this.spriteWidth));
        this.frames.coordinate.push(new FrameCoordinate(this.spriteWidth  , this.spriteWidth , this.spriteWidth, this.spriteWidth));
        this.frames.coordinate.push(new FrameCoordinate(2*this.spriteWidth  , this.spriteWidth , this.spriteWidth, this.spriteWidth));
        this.frames.coordinate.push(new FrameCoordinate(3*this.spriteWidth  , this.spriteWidth , this.spriteWidth, this.spriteWidth));
        this.frames.coordinate.push(new FrameCoordinate(4*this.spriteWidth  , this.spriteWidth , this.spriteWidth, this.spriteWidth));
        this.frames.coordinate.push(new FrameCoordinate(5*this.spriteWidth  , this.spriteWidth , this.spriteWidth, this.spriteWidth));
        this.frames.coordinate.push(new FrameCoordinate(6*this.spriteWidth  , this.spriteWidth , this.spriteWidth, this.spriteWidth));
        this.frames.coordinate.push(new FrameCoordinate(7*this.spriteWidth  , this.spriteWidth , this.spriteWidth, this.spriteWidth));

        this.frames.coordinate.push(new FrameCoordinate(0 , 2*this.spriteWidth , this.spriteWidth, this.spriteWidth));
        this.frames.coordinate.push(new FrameCoordinate(this.spriteWidth , 2*this.spriteWidth , this.spriteWidth, this.spriteWidth));
        this.frames.coordinate.push(new FrameCoordinate(2*this.spriteWidth , 2*this.spriteWidth , this.spriteWidth, this.spriteWidth));
        this.frames.coordinate.push(new FrameCoordinate(3*this.spriteWidth , 2*this.spriteWidth , this.spriteWidth, this.spriteWidth));
        this.frames.coordinate.push(new FrameCoordinate(4*this.spriteWidth , 2*this.spriteWidth , this.spriteWidth, this.spriteWidth));
        this.frames.coordinate.push(new FrameCoordinate(5*this.spriteWidth , 2*this.spriteWidth , this.spriteWidth, this.spriteWidth));
    }

    public actionLoop(startTime: number): void{
        this.moveVector.clear();
        
        if(this.stopY >= this.positionY + this.moveVector.y)
            this.moveVector.y = this.speed;       
        else{
            this.positionY = this.stopY;
            this.moveVector.y = 0;
        } 

        this.move(this.moveVector); 
        this.endTime = performance.now();
        this.update(this.endTime - startTime);
    }
}