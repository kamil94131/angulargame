import { DynamicAnimationContinuous } from "../animations/dynamic-animation-continuous";
import { FrameCoordinate } from "../frame-coordinate";
import { MoveVector } from "../move-vector";

export class Explosion extends DynamicAnimationContinuous {
    private moveVector: MoveVector = new MoveVector();
    private endTime: number = 0;
    private speed: number = 0.1;
    private spriteWidth: number = 125;

    constructor(context: CanvasRenderingContext2D){
        super(context);
        this.configureSprite();
        this.loadFrames();
    }

    private configureSprite(): void{
        this.positionX = 600;
        this.positionY = 200;
        this.frameTime = 3;
    }

    public loadFrames(): void{
        this.frames.texture.src = '../assets/images/explosions/explosion.png';
        this.frames.coordinate.push(new FrameCoordinate(0 , 0, this.spriteWidth, this.spriteWidth));
        this.frames.coordinate.push(new FrameCoordinate(this.spriteWidth , 0, this.spriteWidth, this.spriteWidth));
        this.frames.coordinate.push(new FrameCoordinate(2*this.spriteWidth , 0, this.spriteWidth, this.spriteWidth));
        this.frames.coordinate.push(new FrameCoordinate(3*this.spriteWidth , 0, this.spriteWidth, this.spriteWidth));
        this.frames.coordinate.push(new FrameCoordinate(0 , this.spriteWidth, this.spriteWidth, this.spriteWidth));
        this.frames.coordinate.push(new FrameCoordinate(this.spriteWidth  , this.spriteWidth , this.spriteWidth, this.spriteWidth));
        this.frames.coordinate.push(new FrameCoordinate(2*this.spriteWidth  , this.spriteWidth , this.spriteWidth, this.spriteWidth));
        this.frames.coordinate.push(new FrameCoordinate(3*this.spriteWidth  , this.spriteWidth , this.spriteWidth, this.spriteWidth));
        this.frames.coordinate.push(new FrameCoordinate(0 , 2*this.spriteWidth , this.spriteWidth, this.spriteWidth));
        this.frames.coordinate.push(new FrameCoordinate(this.spriteWidth  , 2*this.spriteWidth , this.spriteWidth, this.spriteWidth));
        this.frames.coordinate.push(new FrameCoordinate(2*this.spriteWidth  , 2*this.spriteWidth , this.spriteWidth, this.spriteWidth));
        this.frames.coordinate.push(new FrameCoordinate(3*this.spriteWidth  , 2*this.spriteWidth , this.spriteWidth, this.spriteWidth));
        this.frames.coordinate.push(new FrameCoordinate(0 , 3*this.spriteWidth , this.spriteWidth, this.spriteWidth));
        this.frames.coordinate.push(new FrameCoordinate(this.spriteWidth  , 3*this.spriteWidth , this.spriteWidth, this.spriteWidth));
    }

    public actionLoop(startTime: number): void{
        this.moveVector.clear();
        this.moveVector.x = this.speed; 
        this.move(this.moveVector);          

        this.endTime = performance.now();
        this.update(this.endTime - startTime);
    }
}