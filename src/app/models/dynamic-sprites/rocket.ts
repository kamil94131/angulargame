import { DynamicAnimationContinuous } from "../animations/dynamic-animation-continuous";
import { FrameCoordinate } from "../frame-coordinate";
import { MoveVector } from "../move-vector";

export class Rocket extends DynamicAnimationContinuous {
    private moveVector: MoveVector = new MoveVector();
    private endTime: number = 0;
    private speed: number = 1;
    private change: boolean = false;
    private spriteStartX: number = 3;
    private spriteStartY: number = 272;
    public spriteWidth: number = 25;
    public spriteHeight: number = 62;

    
    constructor(context: CanvasRenderingContext2D){
        super(context);
        this.configureSprite();
        this.loadFrames();
    }

    public configureSprite(): void{
        this.positionX = 0;
        this.positionY = Math.floor(Math.random() * (window.innerHeight - 200)) + 20;
        // this.positionY = 450;
        this.frameTime = 1;
        this.angel = 90;
    }

    public loadFrames(): void{
        this.frames.texture.src = '../assets/images/rockets/rockets.png';
        this.frames.coordinate.push(new FrameCoordinate(5 , 272, this.spriteWidth, this.spriteHeight));
        this.frames.coordinate.push(new FrameCoordinate(34 , 272, this.spriteWidth, this.spriteHeight));
        this.frames.coordinate.push(new FrameCoordinate(64 , 272, this.spriteWidth, this.spriteHeight));
        this.frames.coordinate.push(new FrameCoordinate(95 , 272, this.spriteWidth, this.spriteHeight));
        this.frames.coordinate.push(new FrameCoordinate(126 , 272, this.spriteWidth, this.spriteHeight));
        this.frames.coordinate.push(new FrameCoordinate(165 , 272, this.spriteWidth, this.spriteHeight));
        this.frames.coordinate.push(new FrameCoordinate(204 , 272, this.spriteWidth, this.spriteHeight));
        this.frames.coordinate.push(new FrameCoordinate(238 , 272, this.spriteWidth, this.spriteHeight));
        this.frames.coordinate.push(new FrameCoordinate(270 , 272, this.spriteWidth, this.spriteHeight));
        this.frames.coordinate.push(new FrameCoordinate(299 , 272, this.spriteWidth, this.spriteHeight));
        this.frames.coordinate.push(new FrameCoordinate(328 , 272, this.spriteWidth, this.spriteHeight));
        this.frames.coordinate.push(new FrameCoordinate(356 , 272, this.spriteWidth, this.spriteHeight));
    }

    public actionLoop(startTime: number): void{
        this.moveVector.clear();
        this.moveVector.x = this.speed; 
        this.move(this.moveVector);

        if(this.angel > 95  || this.angel < 85)
            this.change = !this.change;

        if(this.change)
            this.angel++;
        else
            this.angel--;              

        if(this.positionX > window.innerWidth){
            this.positionX = 0;
            this.positionY = Math.floor(Math.random() * (window.innerHeight - 200)) + 20;
        }
            

        this.endTime = performance.now();
        this.update(this.endTime - startTime);
    }
}