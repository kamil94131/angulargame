import { Frames } from "../frames";
import { FrameCoordinate } from "../frame-coordinate";
import { MoveVector } from "../move-vector";
import { AstronautAnimationName } from "../util/astronaut-animation-name";
import { KeyCode } from "../util/key-code";
import { DynamicAnimationNotContinuous } from "../animations/dynamic-animtion-not-continuous";

export class Astronaut extends DynamicAnimationNotContinuous {
    private currentAnimation: Frames;
    private astronautFrames: Frames[];
    private moveVector: MoveVector = new MoveVector();
    private speed: number = 2;

    private endTime: number = 0;
    private startTime: number = 0;
    private keyCode: number;
    private wasKeyRightPressed: boolean = false;
    private wasKeyLeftPressed: boolean = false;
    private jumpWasPressed: boolean = false;
    private jumpIteration: number = 0;
    private deltaTime: number = 0;
    private firstJump: boolean = true;
    public health: number = 1000;

    constructor(context: CanvasRenderingContext2D){
        super(context);
        this.configureSprite();
        this.createSprite();
        this.initializeWindowEvent();
    }

    private configureSprite(): void{
        this.positionX = 800;
        this.positionY = 400;
        this.frameTime = 0.5;
    }

    private createSprite(): void{
        this.astronautFrames = this.loadFrames();
        this.currentAnimation = this.astronautFrames[AstronautAnimationName.WALK_DOWN];
        this.play(this.currentAnimation);
    }    

    private loadFrames(): Frames[]{
        var framesContainer: Frames[] = [];

        var walkDown: Frames = new Frames();
        var walkUp: Frames = new Frames();
        var walkLeft: Frames = new Frames();
        var walkRight: Frames = new Frames();

        walkDown.texture.src = '../assets/images/astronaut/sprite.bmp';
        walkLeft.texture.src = '../assets/images/astronaut/sprite.bmp';
        walkRight.texture.src = '../assets/images/astronaut/sprite.bmp';
        walkUp.texture.src = '../assets/images/astronaut/sprite.bmp';

        walkDown.coordinate.push(new FrameCoordinate(0, 0, 32, 32));
        walkDown.coordinate.push(new FrameCoordinate(32, 0, 32, 32));
        walkDown.coordinate.push(new FrameCoordinate(64, 0, 32, 32)); 
        framesContainer.push(walkDown);

        walkLeft.coordinate.push(new FrameCoordinate(0, 32, 32, 32));
        walkLeft.coordinate.push(new FrameCoordinate(32, 32, 32, 32));
        walkLeft.coordinate.push(new FrameCoordinate(64, 32, 32, 32)); 
        framesContainer.push(walkLeft);

        walkRight.coordinate.push(new FrameCoordinate(0, 64, 32, 32));
        walkRight.coordinate.push(new FrameCoordinate(32, 64, 32, 32));
        walkRight.coordinate.push(new FrameCoordinate(64, 64, 32, 32));
        framesContainer.push(walkRight);

        walkUp.coordinate.push(new FrameCoordinate(0, 96, 32, 32));
        walkUp.coordinate.push(new FrameCoordinate(32, 96, 32, 32));
        walkUp.coordinate.push(new FrameCoordinate(64, 96, 32, 32)); 
        framesContainer.push(walkUp);
        
        return framesContainer;
    }

    private initializeWindowEvent(): void {
        window.addEventListener('keydown',this.onKeyDownEvent.bind(this), true);
        window.addEventListener("keyup", this.onKeyUpEvent.bind(this), true);
    }

    public actionLoop(startTime: number): void{
        this.moveVector.clear();

        if(this.wasKeyLeftPressed){
            this.currentAnimation = this.astronautFrames[AstronautAnimationName.WALK_LEFT];
            this.moveVector.x = -this.speed;       
        }
        if(this.wasKeyRightPressed){
            this.currentAnimation = this.astronautFrames[AstronautAnimationName.WALK_RIGHT];
            this.moveVector.x = this.speed;             
        }  

        this.endTime = performance.now();

        if(this.jumpWasPressed){
            this.deltaTime = this.deltaTime + this.endTime - startTime;
            if(this.deltaTime > 1 && this.jumpIteration < 90){
                this.moveVector.y = -this.speed-1;
                this.jumpIteration += 1;
            }else if(this.deltaTime > 1 && this.jumpIteration < 180){
                this.moveVector.y = this.speed+1;
                this.jumpIteration += 1;
            }else if(this.jumpIteration >= 100){
                this.jumpIteration = 0;
                this.jumpWasPressed = false;
            }else if(this.firstJump){
                this.deltaTime = 1;
                this.moveVector.y = -this.speed-1;
                this.jumpIteration += 1;
                this.firstJump = false;
            }
        }else{
            this.moveVector.y += this.speed;
        }

        if(this.chceckGroundCollision())
            this.moveVector.y = 0;
        this.endTime = performance.now();
        this.play(this.currentAnimation);
        this.move(this.moveVector);     
        this.update(this.endTime - startTime);

        if(!this.wasKeyLeftPressed && !this.wasKeyRightPressed){
            this.stop(1);
        }           
    }

    public decrementHealth(){
        if(this.health != 0){
            var delta = Math.floor(Math.random() * 50) + 50;
            if(this.health - delta > 0)
                this.health -= delta;
            else
                this.health = 0;
        }      
    }

    public onKeyDownEvent(event: KeyboardEvent): void {    
        if(event.keyCode == KeyCode.KEY_LEFT)
            this.wasKeyLeftPressed = true;
        if(event.keyCode == KeyCode.KEY_RIGHT)
            this.wasKeyRightPressed = true;
        if(event.keyCode == KeyCode.KEY_UP)
            this.jumpWasPressed = true;
    }

    public onKeyUpEvent(event: KeyboardEvent): void{
        if(event.keyCode == KeyCode.KEY_LEFT)  
            this.wasKeyLeftPressed = false;
        if(event.keyCode == KeyCode.KEY_RIGHT)
            this.wasKeyRightPressed = false;
    }

    public chceckGroundCollision(): boolean{
        if(this.positionY + this.frames.coordinate[0].height + this.moveVector.y >= 657 && this.positionY < 657 && this.moveVector.y > 0){
            if(this.moveVector.y > 0)
                this.positionY = 657 - this.frames.coordinate[0].height+2;
            return true
        }

        if(this.positionY + this.frames.coordinate[0].height + this.moveVector.y >= 450 && this.positionY < 450  && this.moveVector.y > 0){
           if(this.positionX <= 464 && this.positionX + this.frames.coordinate[0].width >= 30){
                if(this.moveVector.y > 0)
                    this.positionY = 450 - this.frames.coordinate[0].height+2;
                return true;
           }
        }

        if(this.positionY + this.frames.coordinate[0].height + this.moveVector.y >= 250 && this.positionY < 250  && this.moveVector.y > 0){
            if(this.positionX <= 800 && this.positionX + this.frames.coordinate[0].width >= 628){
                if(this.moveVector.y > 0)
                    this.positionY = 250 - this.frames.coordinate[0].height+2;
                return true;
            }
         }

         if(this.positionY + this.frames.coordinate[0].height + this.moveVector.y >= 200 && this.positionY < 200){
            if(this.positionX <= 1478 && this.positionX + this.frames.coordinate[0].width >= 1070){
                if(this.moveVector.y > 0)   
                    this.positionY = 200 - this.frames.coordinate[0].height+2;
                return true;
            }
         }

        return false;
    }


}