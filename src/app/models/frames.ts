import { FrameCoordinate } from "./frame-coordinate";

export class Frames {
    private _texture: HTMLImageElement = new Image();
	private _coordinate: Array<FrameCoordinate> = new Array<FrameCoordinate>();
	
	constructor(){}

	public get texture(): HTMLImageElement {
		return this._texture;
	}

	public set texture(texture: HTMLImageElement) {
		this._texture = texture;
    }
    
	public get coordinate(): Array<FrameCoordinate> {
		return this._coordinate;
	}

	public set coordinate(coordinate: Array<FrameCoordinate>) {
		this._coordinate = coordinate;
	}	
}

