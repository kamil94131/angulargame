export class MoveVector {
    public x: number = 0;
    public y: number = 0;

    public clear(){
        this.x = 0;
        this.y = 0;
    }
}