export class AstronautAnimationName {
    public static readonly WALK_DOWN = 0;
    public static readonly WALK_LEFT = 1;
    public static readonly WALK_RIGHT = 2;
    public static readonly WALK_UP = 3;
}