export class KeyCode {
    public static readonly KEY_DOWN: number = 40;
    public static readonly KEY_UP: number = 38;
    public static readonly KEY_LEFT: number = 37;
    public static readonly KEY_RIGHT: number = 39;
    public static readonly SPACE: number = 32;
}
