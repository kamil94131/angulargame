import { Frames } from "../frames";
import { MoveVector } from "../move-vector";
import { IAnimation } from "./i-animation";

export class DynamicAnimationNotContinuous implements IAnimation {   
    private context: CanvasRenderingContext2D;

    private _frames: Frames;
    private _frameTime: number = 0;
    private _currentFrameTime: number = 0;
    private _positionX: number = 0;
    private _positionY: number = 0;
    private _frameIndex: number = 0;
    private _paused: boolean = true;   

    constructor(context: CanvasRenderingContext2D){
        this.context = context;
    }

    public play(frames: Frames): void {
        if(this._frames != frames){
            this._frames = frames;
            this._currentFrameTime = 0;
        }   

        this._paused = false;
    }

    public pause(): void {
        this._paused = true;
    }

    public stop(stopFrameIndex: number): void {
        this._paused = true;
        this._frameIndex = stopFrameIndex;
        this._currentFrameTime = 0;
    }

    public update(time: number): void {
        if(!this._paused && this._frames != null){
            this._currentFrameTime += time;
            
            if(this._currentFrameTime >= this._frameTime){
                this._currentFrameTime = 0;

                if(this._frameIndex + 1 < this._frames.coordinate.length)
                    this._frameIndex++;
                else{
                    this._frameIndex = 0;
                }
            }
        }
    }

    public move(moveVector: MoveVector): void {
        this._positionX += moveVector.x;
        this._positionY += moveVector.y;   
    }
    public draw(): void{
        this.context.save();
        this.context.drawImage(this._frames.texture,
                               this._frames.coordinate[this._frameIndex].x,
                               this._frames.coordinate[this._frameIndex].y,
                               this._frames.coordinate[this._frameIndex].width,
                               this._frames.coordinate[this._frameIndex].height,
                               this._positionX,
                               this._positionY,
                               this._frames.coordinate[this._frameIndex].width,
                               this._frames.coordinate[this._frameIndex].height);                           
        this.context.restore();
    }

	public get frames(): Frames {
		return this._frames;
	}

	public set frames(frames: Frames) {
		this._frames = frames;
	}
	 
	public get frameTime(): number {
		return this._frameTime;
	}

	public set frameTime(frameTime: number) {
		this._frameTime = frameTime;
    } 

	public get positionX(): number {
		return this._positionX;
	}

	public set positionX(positionX: number) {
		this._positionX = positionX;
	}

	public get positionY(): number {
		return this._positionY;
	}

	public set positionY(positionY: number) {
		this._positionY = positionY;
    }  
    
	public get frameIndex(): number  {
		return this._frameIndex;
	}

	public set frameIndex(frameIndex: number ) {
		this._frameIndex = frameIndex;
	} 
}