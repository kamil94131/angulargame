
import { Frames } from "../frames";
import { MoveVector } from "../move-vector";
import { IAnimation } from "./i-animation";

export class StaticAnimation implements IAnimation {
    private context: CanvasRenderingContext2D;

    private _frames: Frames = new Frames();
    private _positionX: number[] = [];
    private _positionY: number[] = [];

    constructor(context: CanvasRenderingContext2D){
        this.context = context;
    }

    public draw(): void{
        for(let frameIndex = 0; frameIndex < this._frames.coordinate.length; frameIndex++){
            this.context.drawImage(this._frames.texture,
                                    this._frames.coordinate[frameIndex].x,
                                    this._frames.coordinate[frameIndex].y,
                                    this._frames.coordinate[frameIndex].width,
                                    this._frames.coordinate[frameIndex].height,
                                    this._positionX[frameIndex],
                                    this._positionY[frameIndex],
                                    this._frames.coordinate[frameIndex].width,
                                    this._frames.coordinate[frameIndex].height);                           
        }    
    }

	public get frames(): Frames {
		return this._frames;
	}

	public set frames(frames: Frames) {
		this._frames = frames;
	}

	public get positionX(): number[]  {
		return this._positionX;
	}

	public set positionX(value: number[] ) {
		this._positionX = value;
	}

	public get positionY(): number[]  {
		return this._positionY;
	}

	public set positionY(value: number[] ) {
		this._positionY = value;
	}
}