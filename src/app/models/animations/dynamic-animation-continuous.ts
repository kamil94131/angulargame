import { Frames } from "../frames";
import { MoveVector } from "../move-vector";
import { IAnimation } from "./i-animation";

export class DynamicAnimationContinuous implements IAnimation {
    private context: CanvasRenderingContext2D;

    private _frames: Frames = new Frames();
    private _frameTime: number = 0;
    private _currentFrameTime: number = 0;
    private _positionX: number = 0;
    private _positionY: number = 0;
    private _frameIndex: number = 0;  
    private _angel: number = 0;

    constructor(context: CanvasRenderingContext2D){
        this.context = context;
    }

    public update(time: number): void {
        if(this._frames != null){
            this._currentFrameTime += time;
            
            if(this._currentFrameTime >= this._frameTime){
                this._currentFrameTime = 0;

                if(this._frameIndex + 1 < this._frames.coordinate.length)
                    this._frameIndex++;
                else{
                    this._frameIndex = 0;
                }
            }
        }
    }

    public move(moveVector: MoveVector): void {
        this._positionX += moveVector.x;
        this._positionY += moveVector.y;   
    }
    
    public draw(): void{
        this.context.drawImage(this._frames.texture,
                               this._frames.coordinate[this._frameIndex].x,
                               this._frames.coordinate[this._frameIndex].y,
                               this._frames.coordinate[this._frameIndex].width,
                               this._frames.coordinate[this._frameIndex].height,
                               this._positionX,
                               this._positionY,
                               this._frames.coordinate[this._frameIndex].width,
                               this._frames.coordinate[this._frameIndex].height);
        this.context.restore();
    }

    public drawWithRotate() : void {
        this.context.save();
        this.context.translate(this._positionX + (this._frames.coordinate[this._frameIndex].width/2), 
                               this._positionY + (this._frames.coordinate[this._frameIndex].height/2));
        this.context.rotate(this.angel*Math.PI/180);
        this.context.drawImage(this._frames.texture,
                                this._frames.coordinate[this._frameIndex].x,
                                this._frames.coordinate[this._frameIndex].y,
                                this._frames.coordinate[this._frameIndex].width,
                                this._frames.coordinate[this._frameIndex].height,
                                -this._frames.coordinate[this._frameIndex].width/2,
                                -this._frames.coordinate[this._frameIndex].height/2,
                                this._frames.coordinate[this._frameIndex].width,
                                this._frames.coordinate[this._frameIndex].height);
        this.context.restore();
    }

	public get frames(): Frames {
		return this._frames;
	}

	public set frames(frames: Frames) {
		this._frames = frames;
	}
	 
	public get frameTime(): number {
		return this._frameTime;
	}

	public set frameTime(frameTime: number) {
		this._frameTime = frameTime;
    } 

	public get positionX(): number {
		return this._positionX;
	}

	public set positionX(positionX: number) {
		this._positionX = positionX;
	}

	public get positionY(): number {
		return this._positionY;
	}

	public set positionY(positionY: number) {
		this._positionY = positionY;
    }  
    
	public get frameIndex(): number  {
		return this._frameIndex;
	}

	public set frameIndex(frameIndex: number ) {
		this._frameIndex = frameIndex;
    }     

	public get angel(): number  {
		return this._angel;
	}

	public set angel(value: number ) {
		this._angel = value;
	} 
}