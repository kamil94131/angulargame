export interface IAnimation {
    draw(): void;
}