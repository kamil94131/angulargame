import { Component, OnInit, NgZone, ChangeDetectorRef } from '@angular/core';
import { Astronaut } from './models/dynamic-sprites/astronaut';
import { Background } from './models/static-sprite/background';
import { Ground } from './models/static-sprite/ground';
import { Rocket } from './models/dynamic-sprites/rocket';
import { Explosion } from './models/dynamic-sprites/explosion';
import { PACKAGE_ROOT_URL } from '@angular/core/src/application_tokens';
import { Coin } from './models/dynamic-sprites/coin';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    private canvas: HTMLCanvasElement;
    private context: CanvasRenderingContext2D; 

    private astrounaut: Astronaut;
    private background: Background;
    private ground: Ground;
    private rockets: Rocket[] = [];
    private rocketsNumber: number = 1;
    private rocketTime: number = 0;
    private explosions: Explosion[] = [];
    private gameOver: boolean = false;
    private coins: Coin[] = [];
    private pointsNumber: number = 0;

    private startTime: number = 0;

    constructor(private ngZone: NgZone, private ref: ChangeDetectorRef){
        this.initialize();
    }

    private initialize(): void {  
        onload = () => {         
            this.initializeCanvas();    
            this.initializeContext();
            this.creatGameSprites();                 
            this.gameLoop(); 
            this.ref.detach();
            this.ngZone.runOutsideAngular(() => this.gameLoop());
        }
    }

    private initializeCanvas(): void {
        this.canvas = <HTMLCanvasElement>document.getElementById('canvas');
        this.canvas.height = window.innerHeight;
        this.canvas.width = window.innerWidth;
    }

    private initializeContext(): void {
        this.context = this.canvas.getContext("2d");
    }

    private creatGameSprites(): void {
        this.astrounaut = new Astronaut(this.context);
        this.background = new Background(this.context);
        this.ground = new Ground(this.context);

        for(let i = 0; i < 30; i++)
            this.coins.push(new Coin(this.context));

        for(let i = 0; i < 15; i++)
            this.rockets.push(new Rocket(this.context));

        for(let i = 0; i < 1; i++)
            this.explosions.push(new Explosion(this.context));
    }

    private gameLoop(): void {   
        this.startTime = performance.now();              
        this.context.clearRect(0, 0, window.innerWidth, window.innerHeight);
        this.background.draw();     
        if(!this.gameOver){
            this.astrounaut.actionLoop(this.startTime);
            this.astrounaut.draw();
        }

        this.ground.draw();

        this.rocketTime = this.rocketTime + (performance.now() - this.startTime);

        if(this.rocketTime > 100 && this.rocketsNumber < 15){
            this.rocketsNumber = this.rocketsNumber + 1;
            this.rocketTime = 0;
        }        

        for(let i = 0; i < this.rocketsNumber; i++){
            this.rockets[i].actionLoop(this.startTime);
            this.rockets[i].drawWithRotate();
        }

        for(let i = 0; i < this.explosions.length; i++){
        
            if(this.explosions[i].frameIndex == this.explosions[i].frames.coordinate.length - 1){
                this.explosions.splice(i, 1);
            }else{
                this.explosions[i].actionLoop(this.startTime);
                this.explosions[i].draw();
            }       
        }

        for(let i = 0; i < this.coins.length; i++){
            this.coins[i].actionLoop(this.startTime);
            this.coins[i].draw();
        }

        this.context.font = "64px Comic Sans MS";
        this.context.fillStyle = "red";
        this.context.textAlign = "center";

        if(!this.gameOver)
            this.context.fillText("Health: " + this.astrounaut.health, 550, 740); 
        else
            this.context.fillText("Game over", 550, 740);

        this.context.font = "64px Comic Sans MS";
        this.context.fillStyle = "blue";
        this.context.textAlign = "center";

        if(!this.gameOver)
            this.context.fillText("Points: " + this.pointsNumber,950, 740);
        else

        this.context.fillText("Result: " + this.pointsNumber,950, 740);
        this.checkCollision();

        requestAnimationFrame(()=>this.gameLoop());
    }

    private checkCollision(){
        for(let i = 0; i < this.rocketsNumber; i++){
            this.checkIfGroundCollision(i, this.rockets[i], this.ground.positionX[14], this.ground.positionY[14],
                                        this.ground.frames.coordinate[14].width,this.ground.frames.coordinate[14].height);
        }
        for(let i = 0; i < this.rocketsNumber; i++){
            this.checkIfGroundCollision(i, this.rockets[i], this.ground.positionX[18], this.ground.positionY[18],
                                        this.ground.frames.coordinate[18].width,this.ground.frames.coordinate[18].height);
        }
        for(let i = 0; i < this.rocketsNumber; i++){
            this.checkIfGroundCollision(i, this.rockets[i], this.ground.positionX[20], this.ground.positionY[20],
                                        this.ground.frames.coordinate[20].width,this.ground.frames.coordinate[20].height);
        }      
        
        for(let i = 0; i < this.rocketsNumber; i++){
            if(this.checkIfGroundCollision(i, this.rockets[i], this.astrounaut.positionX, this.astrounaut.positionY,
                                        this.astrounaut.frames.coordinate[0].width,this.astrounaut.frames.coordinate[0].height)){
                this.astrounaut.decrementHealth();
                if(this.astrounaut.health == 0){
                    this.gameOver = true;
                    this.astrounaut.positionY = window.innerHeight - this.astrounaut.frames.coordinate[0].height;
                }
                     
            }
        }
        for(let i=0; i< this.coins.length; i++){
            if(this.astrounaut.positionX + this.astrounaut.frames.coordinate[0].width >= this.coins[i].positionX &&
                this.astrounaut.positionX <= this.coins[i].positionX + this.coins[i].spriteWidth){
                 if(this.coins[i].positionY <= this.astrounaut.positionY + this.astrounaut.frames.coordinate[0].height  && this.coins[i].positionY + this.coins[i].spriteHeight >= this.astrounaut.positionY +this.astrounaut.frames.coordinate[0].height){
                    this.pointsNumber += 10;
                    this.coins[i].positionY = 0;
                    this.coins[i].positionX = Math.floor(Math.random() * 1400) + 10;
                    this.coins[i].c();
                }
            }
        }   
    }

    private checkIfGroundCollision(index: number, rocket: Rocket, x: number, y: number, width: number, height: number): boolean{
        if(rocket.positionX + rocket.spriteWidth >= x && rocket.positionX + rocket.spriteWidth <= x + 112) {
            let tmp: boolean = false;
            if(rocket.positionY >= y && rocket.positionY <= y + height){
                this.rockets.splice(index, 1);
                var explosion: Explosion = new Explosion(this.context);
                explosion.positionX = rocket.positionX;
                explosion.positionY = rocket.positionY;
                this.explosions.push(explosion);
                this.rockets.push(new Rocket(this.context));
                tmp = true;
            }
            if(rocket.positionY + rocket.spriteHeight >= y && rocket.positionY + rocket.spriteHeight <= y + height){
                this.rockets.splice(index, 1);
                var explosion: Explosion = new Explosion(this.context);
                explosion.positionX = rocket.positionX;
                explosion.positionY = rocket.positionY;
                this.explosions.push(explosion);
                this.rockets.push(new Rocket(this.context));
                tmp = true;
            }
            return tmp;
        }
        return false;
    }
}


